const Url = {
	// wx
	Login: '/wx/login', // 登录
	Test: '/wx/test', // 测试

	// 宝宝信息
	BabyList: '/baby/list', // get
	BabyGet: '/baby/get', // get
	BabyAdd: '/baby/add', // post
	BabyEdit: '/baby/edit', // put
	BabyDelete: '/baby/delete', // delete
	
	// 吸奶器
	SuckleList: '/babyFoodBreastPump/list', // get
	SuckleGet: '/babyFoodBreastPump/get', // get
	SuckleAdd: '/babyFoodBreastPump/add', // post
	SuckleEdit: '/babyFoodBreastPump/edit', // put
	SuckleDelete: '/babyFoodBreastPump/delete', // delete
	
	// 亲喂
	NurseList: '/babyFoodFeedingMother/list', // get
	NurseGet: '/babyFoodFeedingMother/get', // get
	NurseAdd: '/babyFoodFeedingMother/add', // post
	NurseEdit: '/babyFoodFeedingMother/edit', // put
	NurseDelete: '/babyFoodFeedingMother/delete', // delete
	
	// 奶瓶喂养
	BottleList: '/babyFoodFeedingBottle/list', // get
	BottleGet: '/babyFoodFeedingBottle/get', // get
	BottleAdd: '/babyFoodFeedingBottle/add', // post
	BottleEdit: '/babyFoodFeedingBottle/edit', // put
	BottleDelete: '/babyFoodFeedingBottle/delete', // delete
	
	// 换尿布
	DiaperList: '/babyDiaper/list', // get
	DiaperGet: '/babyDiaper/get', // get
	DiaperAdd: '/babyDiaper/add', // post
	DiaperEdit: '/babyDiaper/edit', // put
	DiaperDelete: '/babyDiaper/delete', // delete
}

// 接口声明区
export default {
	Url
}
